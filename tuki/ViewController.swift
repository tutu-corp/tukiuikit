//
//  ViewController.swift of project named tuki
//
//  Created by Aurore D (@Tuwleep) on 13/03/2023.
//
//  

import UIKit

class ViewController: UIViewController {

    
    // [job] [object] depuis [date]
    var job = ["Cultivateur","Éducateur", "Pécheur", "Créateur", "Gestionnaire", "Voleur", "Chasseur", "Consommateur", "Revendeur"]
    var object = ["de carottes", "de poissons", "d'abeilles", "de mensonges", "de fromages", "de cailloux", "d'ignorants", ]
    var date = ["2019", "1993", "700 av JC", "hier"]
    
    @IBOutlet weak var quoteLabel: UILabel!

    @IBAction func changeQuote(_ sender: Any) {
        print("It's ok")
        
        let randomIndexObject = Int.random(in: 0..<object.count)
        let randomObject = object[randomIndexObject]
        
        let randomIndexJob = Int.random(in: 0..<job.count)
        let randomJob = job[randomIndexJob]
        
        let randomIndexDate = Int.random(in: 0..<date.count)
        let randomDate = date[randomIndexDate]
        
        print(randomJob)
        print(randomObject)
        print(randomDate)
        
        quoteLabel.text = "\(randomJob) \(randomObject) depuis \(randomDate)"
    }
}




